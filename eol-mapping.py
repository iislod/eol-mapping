"""eol-mapping

Map the Title field to pages on eol.org.
"""
import os
import csv
import json
import time
from argparse import RawTextHelpFormatter, ArgumentParser

import requests


parser = ArgumentParser(formatter_class=RawTextHelpFormatter)
parser.add_argument("-f", metavar="INPUT_TERMS_FILE",
                    help="the terms csv file", required=True)
parser.add_argument("-e", metavar="INPUT_ERRORS_FILE",
                    help="the errors csv file", required=False)
parser.add_argument("-k", metavar="API_KEY",
                    help="the api key of EOL", required=True)
parser.add_argument("-o", metavar="OUTPUT_FILE",
                    help="the output csv file", required=True)
args = parser.parse_args()


def eol_search(oid, name):
    """Search eol.org by title field using eol's search API. If the search is failed, record the OID and the status code.
    """
    output = []

    if not name:
        return output

    payload = {"q": name, "exact": True, "format": "json", "key": args.k}
    r = requests.get("http://eol.org/api/search/1.0/", params=payload)

    if r.status_code == 200:
        result_list = json.loads(r.text)["results"]
        if result_list:
            output = result_list
    else:
        api_error_list.append(
            {"OID": oid, "name": name, "code": r.status_code})

    return output


def eol_pages(oid, id):
    """Get the details of a eol page using eol's pages API. If the process is failed, record the OID and the status code.
    """
    output = []
    payload = {"id": int(id), "images": 0, "videos": 0, "text": 0,
               "common_names": True, "format": "json", "key": args.k}
    r = requests.get("http://eol.org/api/pages/1.0/", params=payload)

    if r.status_code == 200:
        result_list = json.loads(r.text)["vernacularNames"]
        if result_list:
            output = [result["vernacularName"] + "|" + result["language"]
                      for result in result_list]
    else:
        api_error_list.append({"OID": oid, "id": id, "code": r.status_code})

    return output


s_time = time.time()
out_rows = []
api_error_list = []
ids = set()

input_r = csv.DictReader(open(os.path.normpath(args.f), encoding="utf-8-sig"))

if args.e:
    input_e = csv.DictReader(open(args.e, encoding="utf-8-sig"))
    for row in input_e:
        if row["code"] == "404":
            continue
        ids.add(row["OID"])

for row in input_r:
    if ids and row["OID"] not in ids:
        out_rows.append(row)
        continue
    scientific_name = row["Terms::latin"]
    search_results = eol_search(row["OID"], scientific_name)
    if search_results:
        search_result = search_results[0]
        row.update({"eol_id": search_result["id"], "eol_title": search_result[
                   "title"], "eol_link": search_result["link"], "eol_content": search_result["content"]})
        page_result = eol_pages(row["OID"], search_result["id"])
        if page_result:
            row["eol_vernacular_names"] = ";".join(page_result)
    else:
        eol_ids_c = set()
        eol_vernacular_names_c = set()
        for chinese_name in row["Terms::zh"].split(";"):
            c_search_results = eol_search(row["OID"], chinese_name)
            if c_search_results:
                for c_search_result in c_search_results:
                    eol_ids_c.add(str(c_search_result["id"]))
                    c_page_result = eol_pages(row["OID"], c_search_result["id"])
                    if c_page_result:
                        eol_vernacular_names_c.add(";".join(c_page_result))
        if eol_ids_c:
            row["eol_ids_c"] = ";".join(list(eol_ids_c))
        if eol_vernacular_names_c:
            row["eol_vernacular_names_c"] = ";".join(
                list(eol_vernacular_names_c))
    out_rows.append(row)

# If input csv does not have eol fields (which means input csv is not
# eol-matched csv to be fixed), add eol fields to it.
if "eol_id" not in input_r.fieldnames:
    input_r.fieldnames += ["eol_id", "eol_title", "eol_link", "eol_content",
                           "eol_vernacular_names", "eol_ids_c", "eol_vernacular_names_c"]

# Ouput
# Write the matching result.
os.makedirs(os.path.dirname(os.path.abspath(args.f)), exist_ok=True)
with open(os.path.normpath(args.o), "x", newline="", encoding="utf-8-sig") as fou:
    dw = csv.DictWriter(fou, fieldnames=input_r.fieldnames, dialect="unix")
    dw.writeheader()
    dw.writerows(out_rows)

# Write the error list.
if api_error_list:
    with open(os.path.normpath(args.o[:-4] + "_Error_list_eol_search.csv"), "x", newline="") as fou:
        dw = csv.DictWriter(fou, fieldnames=["OID", "id", "name", "code"])
        dw.writeheader()
        dw.writerows(api_error_list)

print(time.time() - s_time)
