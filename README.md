# eol-mapping: Tool for EOL.org Mapping

author: [Cheng-Jen Lee](http://about.me/Sollee)

# Introduction

This tool is designed to map the `Title` field to pages on [Encyclopedia of Life](http://eol.org/). It can also recovery lost data caused by EOL's API error.

## Input

* 1. The following fields from csv files converted from xml files of Union Catalog of Digital Archives Taiwan:

| Field | Value |
| ----- | ----- |
| OID | (from input csv) |
| Terms::latin | latin names extracted from the field `Title` in the csv file |
| Terms::zh | Chinese names extracted from the field `Title` in the csv file |

* 2. The list of records (OIDs) with errors caused by EOL's API error (optional, used for recoverying lost data):

| Field | Value |
| ----- | ----- |
| OID | (from input csv) |
| id | EOL id (record when there is an error in running [EOL's pages api](http://eol.org/api/docs/pages)) |
| name | Chinese names extracted from the field `Title` in the csv file (record when there is an error in running [EOL's search api](http://eol.org/api/docs/search)) |
| code | the HTTP status code |

## Output

A csv file with all fields from the input csv file and the following fields:

| Field | Value |
| ----- | ----- |
| eol\_id | EOL id returned by searching `Terms::latin` using EOL's search API |
| eol\_title | EOL title returned by searching `Terms::latin` using EOL's search API |
| eol\_link | EOL link returned by searching `Terms::latin` using EOL's search API |
| eol\_content | EOL content returned by searching `Terms::latin` using EOL's search API |
| eol_vernacular\_names | EOL vernacularNames returned by searching `eol_id` using EOL's pages API, in the form of <code>(vernacularName)&#124;(language)</code> |
| eol\_ids\_c | EOL ids returned by searching `Terms::zh` using EOL's search API |
| eol\_vernacular\_names\_c | EOL vernacularNames returned by searching `eol_ids_c` using EOL's pages API, in the form of <code>(vernacularName)&#124;(language)</code> |

# Dependencies

* Python 3.4

* Python dependencies

```
pip3 install -r requirements.txt
```

* Only tested on Ubuntu (>= 14.04)

```
sudo apt-get install build-essential python3-dev
```

# Usage

## Scenario 1: Map the `Title` field to pages on Encyclopedia of Life

* `python3 eol-mapping.py -f INPUT_TERMS_FILE -k API_KEY -o OUTPUT_FILE`
  * INPUT\_TERMS\_FILE: The terms csv file (Input #1 in the introduction).
  * API\_KEY: The [api key](http://eol.org/api/docs#api_key_section) of EOL.
  * OUTPUT\_FILE: The name for the output csv file.

## Scenario 2: Recovery lost data caused by EOL's API error

* `python3 eol-mapping.py -f INPUT_TERMS_FILE -e INPUT_ERRORS_FILE -k API_KEY -o OUTPUT_FILE`
  * INPUT\_TERMS\_FILE: The terms csv file processed by `Scenario 1`.
  * INPUT\_ERRORS\_FILE: The errors csv file (Input #2 in the introduction).
  * API\_KEY: The api key of EOL.
  * OUTPUT\_FILE: The name for the output csv file.
